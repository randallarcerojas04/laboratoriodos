from Lab2 import *
def menu():
    print("MENU\n"
          "1) Registrar datos de personas.\n"
          "2) Recorrer los Nombres registrados.\n"
          "3) Eliminar una persona.\n"
          "4) Consultar el nombre de una persona.\n"
          "5) Cambiar el nombre.\n"
          "6) Documentación.\n"
          "7) Imprimir diccionario.\n"
          "8) Salir.")
    opcion = input("Digite la opcion que desea realizar:")
    if opcion == "1":
        agregarPersona()
        menu()
    elif opcion == "2":
        recorrerNombres()
        menu()
    elif opcion == "3":
        id = input("Ingrese la identificacion de la persona que desea eliminar:")
        eliminarPersona(id)
        menu()
    elif opcion == "4":
        id = input("Ingrese la identificacion de la persona que desea buscar:")
        buscarPersona(id)
        menu()
    elif opcion == "5":
        id = input("Ingrese la identificacion de la persona que desea modificar:")
        nombre = input("Ingrese el nuevo nombre que desea para esta persona:")
        modificarPersona(id,nombre)
        menu()
    elif opcion == "6":
        documentacion()
        menu()
    elif opcion == "7":
        imprimirDiccionario()
        menu()

    elif opcion == "8":
        return
    else:
        print("Opcion invalida")
menu()