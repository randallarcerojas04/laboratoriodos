Personas = {}
def agregarPersona():
    '''Agrega personas al diccionario por medio de su identificacion y nombre completo.'''
    id = input("Ingrese la identificacion de la persona:")
    nombre = input("Ingrese el nombre completo de la persona:")
    estado = False
    for per in Personas:
        if id == per:
            estado = True

    if not estado:
        Personas[id] = nombre
    else:
        print("La identificacion" ,id, "ya existe")



def recorrerNombres():
    '''Se muestra lista de nombres del diccionario '''
    for per in Personas:
        print(Personas[per])


def eliminarPersona(id):
    '''Se eliminará la persona solicitada por medio de la identificación'''
    estado=False
    for per in Personas:
        if id== per:
            estado=True

    if estado:
        del Personas[id]
    else:
        print("La identificacion no existe")



def buscarPersona(id):
    '''Se buscará la persona solicitada por medio de la identificación'''
    estado = False
    for per in Personas:
        if id == per:
            estado = True

    if estado:
        print(Personas[id])
    else:
        print("La identificacion no existe")

def modificarPersona(id,nombre):
    '''Se modificará la persona solicitada por medio de la identificación'''
    estado=False
    for per in Personas:
        if id== per:
            estado=True

    if estado:
        Personas.update({id:nombre})
    else:
        print("La identificacion no existe")

def documentacion():
    '''Se mostrará la documentación de todos los metodos.'''
    print(agregarPersona.__doc__)
    print(recorrerNombres.__doc__)
    print(eliminarPersona.__doc__)
    print(modificarPersona.__doc__)
    print(buscarPersona.__doc__)
    print(documentacion.__doc__)
    print(imprimirDiccionario.__doc__)

def imprimirDiccionario():
    '''Se mostrará la información del diccionario completo'''
    print(Personas)


